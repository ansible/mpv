# mpv

This Ansible role installs `mpv`, sets its config file, installs `mpv-sponsorblock`
(which is awesome!) and creates an XDG desktop entry for itself.

This role depends on my [python3 role](https://codeberg.org/ansible/python3)
(which provides `python3`, `pip3`, and `yt-dlp` among some other things).


## Config mpv

Set the `config` dict somewhere with higher precedence than role defaults (such
as group or host vars), for example:
```
mpv:
  config:
    volume: "20"
    screenshot-dir: "~/screenshots"
```

The key and value above are simply written to `mpv.conf` in `key=val` syntax, so you
can include any [option supported by mpv](https://mpv.io/manual/master/#options).



## Links and notes

+ https://mpv.io
+ https://github.com/po5/mpv_sponsorblock
+ https://github.com/skorokithakis/catt
  `catt`: a Python package that makes it possible to cast from any URL supported by yt-dlp


### Replaced youtube-dl with yt-dlp

The last few months playing any youtube video with `mpv` has been slow as molasses.
Seems this is something many others have observed as well.

> Deliberate sabotage by Google.
> Youtube changed the challenge mini-language by adding a new inline transformation function.

+ [Randomly slow youtube download speed](https://github.com/ytdl-org/youtube-dl/issues/29326), 2021-06-17
+ [VLC descramble function patch for youtube-dl](https://github.com/ytdl-org/youtube-dl/issues/29326#issuecomment-956200172)
+ [Super slow downloads & other problems](https://github.com/ytdl-org/youtube-dl/issues/30173), 2021-10-31
+ [Slow speed download is a youtube restriction due the fact they bann all the videos regards youtube-dl?](https://github.com/ytdl-org/youtube-dl/issues/30119), 2021-10-17

Suggested fix:

+ apply a patch ([explainer by VLC developer)](https://github.com/ytdl-org/youtube-dl/issues/30097#issuecomment-950157377), [VLC issue on descrambler function](https://code.videolan.org/videolan/vlc/-/issues/26174#note_286445))
+ switch from `youtube-dl` to `yt-dlp` ([youtube-dl project sunsetting?](https://github.com/ytdl-org/youtube-dl/issues/29965))

So I decided to uninstall `youtube-dl` and install `yt-dlp` in its stead.
At this point, attempting to play a youtube video with mpv fails:

```
$ mpv https://www.youtube.com/watch?v=P8ma6v5mApQ
[ytdl_hook]
[ytdl_hook] youtube-dl failed: not found or not enough permissions
Failed to recognize file format.
```

`mpv` is obviously looking for `youtube-dl` in the `PATH` and just as obviously not finding it. Fixed.

+ https://www.funkyspacemonkey.com/replace-youtube-dl-with-yt-dlp-how-to-make-mpv-work-with-yt-dlp

Replacing the out-dated `youtube-dl` with the more up-to-date `yt-dlp` fixed the youtube caching issue
I have been experiencing for months.
With that, I see no need to change to the Savoury PPAs for `mpv` or `ffmpeg` (in order to get more recent versions).

+ https://codeberg.org/ansible/python3/issues/2
+ https://launchpad.net/~savoury1/+archive/ubuntu/mpv
+ https://launchpad.net/~savoury1/+archive/ubuntu/ffmpeg4
+ https://launchpad.net/~savoury1/+archive/ubuntu/graphics
+ https://launchpad.net/~savoury1/+archive/ubuntu/multimedia
